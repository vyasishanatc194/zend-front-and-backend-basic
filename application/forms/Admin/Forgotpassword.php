<?php

class Form_Admin_Forgotpassword extends Form_Custom_General {

    public function init() {

        $email = $this->createElement('text', 'email')
                ->setRequired(TRUE)
                ->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Email address should not be blank.')))
                ->addValidator(new Zend_Validate_EmailAddress())
                ->addFilters(array(
                    new Zend_Filter_StringTrim(),
                    new Zend_Filter_StringToLower()
                ))
                ->setAttribs(Array('class' => "span12", "placeholder" => "E-mail address"));
        $email->getValidator('emailAddress')->setMessage("Email address is not valid.", Zend_Validate_EmailAddress::INVALID_FORMAT);
        $this->addElement($email);
    }

}