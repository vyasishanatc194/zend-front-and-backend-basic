<?php

/*
 * This class is used for common same function.
 */

class Model_Base extends Zend_Db_Table {

    //protected $_name = '';
    /**
     * Smart save
     */
    public function save($param, $primaryKeyColumn = '') {

	$this->registry = Zend_Registry::getInstance();
	$fields = $this->info(Zend_Db_Table_Abstract::COLS);
	foreach ($param as $field => $value) {
	    if (!in_array($field, $fields)) {
		unset($param[$field]);
	    }
	}

	try {
	    if (isset($param[$primaryKeyColumn]) && $param[$primaryKeyColumn] != "") {
		$where = $this->getAdapter()->quoteInto($primaryKeyColumn . ' = ?', $param[$primaryKeyColumn]);
		$idValue = $param[$primaryKeyColumn];
		$this->update($param, $where);
	    } else {
		$idValue = $this->insert($param);
	    }
	} catch (Exception $e) {
	    throw new Exception($e);
	}
	return $idValue;
    }

}
